#include "ControllerOutput.hpp"

#include "App.hpp"
#include "Util.hpp"



const std::array<byte, 100>& ControllerOutput::getState() {
    return this->currentState;
}



byte ControllerOutput::getPixel(byte pitch) {
    return this->currentState[pitch];
}

byte ControllerOutput::getPixel(unsigned int x, unsigned int y) {
    return getPixel(Util::coordsToPitch(x, y));
}


void ControllerOutput::setPixel(byte pitch, byte color) {
    if (pitch >= 128) return;
    if (color >= 128) color = 0;

    // If the color is the same as the current color, don't bother changing it
    if (this->currentState[pitch] != color) {
        this->currentState[pitch] = color;
        this->queuedStateChanges[pitch] = color;
    
        // If the color is the same as the last color, don't send extra MIDI signals
        if (this->lastState[pitch] == color) {
            this->queuedStateChanges.erase(pitch);
        }
    }
}

void ControllerOutput::setPixel(unsigned int x, unsigned int y, byte color) {
    setPixel(Util::coordsToPitch(x, y), color);
}



void ControllerOutput::forceClear() {
    for (int i = 0; i < lastState.size(); i++) {
        queuedStateChanges[i] = 0;
    }
}

void ControllerOutput::clear() {
    for (int i = 0; i < currentState.size(); i++) {
        setPixel(i, 0);
    }
}

void ControllerOutput::display() {
    for (auto it : this->queuedStateChanges) {
        byte pitch = it.first;
        byte velocity = it.second;

        // Send MIDI messages [TODO]
        std::vector<byte> message;
        message.push_back(144 + App::midiOutChannel); // Message type (note on) + MIDI channel
        message.push_back(pitch);
        message.push_back(velocity);
        midi->sendMessage(&message);
    }
    this->queuedStateChanges.clear();
    std::copy(currentState.begin(), currentState.end(), lastState.begin());
}



void ControllerOutput::destroyMIDIDevice() {
    if (midi != nullptr) {
        if (midi->isPortOpen()) {
            midi->closePort();
        }
        delete midi;
    }
}

void ControllerOutput::setMIDIDevice(RtMidiOut* device) {
    destroyMIDIDevice();
    this->midi = device;
}

RtMidiOut* ControllerOutput::getMIDIDevice() {
    return midi;
}



ControllerOutput::ControllerOutput() {
    this->currentState = std::array<byte, 100>();
    this->lastState = std::array<byte, 100>();
    this->queuedStateChanges = std::map<byte, byte>();
    this->midi = nullptr;
}

ControllerOutput::ControllerOutput(RtMidiOut* device) : ControllerOutput() {
    this->setMIDIDevice(device);
}

ControllerOutput::~ControllerOutput() {
    this->destroyMIDIDevice();
}
