#pragma once


#include <thread>

#include "Util.hpp"
#include "Pad.hpp"

class App;



class Renderer {


public:
    App* a;


    void init();

    Renderer(App* a);

private:
    static const int margin = 20;
    static const int size = 50;
    static const int distance = 5;

    void updateButton(int x, int y, byte velocity);

    std::thread t;
    void start();
    void loop();


};
