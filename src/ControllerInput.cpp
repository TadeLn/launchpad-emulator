#include "ControllerInput.hpp"

#include "App.hpp"



ControllerInput::MidiMessage::MidiMessage(double timestamp, std::vector<byte> data) {
    this->timestamp = timestamp;
    this->data = data;
}



void ControllerInput::handleMIDIMessage(double timestamp, std::vector<byte> data) {
    this->unhandledMessages.push(MidiMessage(timestamp, data));
}

void ControllerInput::destroyMIDIDevice() {
    if (midi != nullptr) {
        delete midi;
    }
}

void ControllerInput::setMIDIDevice(RtMidiIn* device) {
    destroyMIDIDevice();
    this->midi = device;
}

RtMidiIn* ControllerInput::getMIDIDevice() {
    return midi;
}


bool ControllerInput::pollMessage(MidiMessage& msg) {
    if (unhandledMessages.empty()) return false;
    msg = unhandledMessages.front();
    unhandledMessages.pop();

    byte messageType = msg.data.at(0);
    byte messageChannel = messageType & 0b00001111;
    byte messageTypeOnly = messageType & 0b11110000;
    if (messageChannel == App::midiInChannel) {
        if (messageTypeOnly == 144 || messageTypeOnly == 176) {
            byte pitch = msg.data.at(1);
            byte velocity = msg.data.at(2);
            this->currentValues[pitch] = velocity;
        }
        if (messageTypeOnly == 128) {
            byte pitch = msg.data.at(1);
            byte velocity = 0;
            this->currentValues[pitch] = velocity;
        }
    }
    return true;
}



bool ControllerInput::isPressed(byte pitch) {
    return getVelocity(pitch) != 0;
}

byte ControllerInput::getVelocity(byte pitch) {
    return this->currentValues[pitch];
}



ControllerInput::ControllerInput() {
    this->midi = nullptr;
}

ControllerInput::ControllerInput(RtMidiIn* device) {
    this->setMIDIDevice(device);
}

ControllerInput::~ControllerInput() {
    this->destroyMIDIDevice();
}
