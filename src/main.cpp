#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string.h>
#include <thread>
#include <csignal>


#include "tutils/Exception.hpp"

#include "App.hpp"



void handleSignal(int signum) {
    delete App::inst();
    exit(EXIT_SUCCESS);
}


int main() {
    App* app = new App();
    signal(SIGINT, handleSignal);

    try {
        app->start();
    } catch (tu::Exception& e) {
        e.print();
        return e.getCode();
    }
    delete app;
    return 0;
}
