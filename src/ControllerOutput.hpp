#pragma once


#include <array>
#include <map>
#include <rtmidi/RtMidi.h>

#include "tutils/Pair.hpp"

#include "Util.hpp"



class ControllerOutput {


public:
    const std::array<byte, 100>& getState();

    byte getPixel(byte pitch);
    byte getPixel(unsigned int x, unsigned int y);
    void setPixel(byte pitch, byte color);
    void setPixel(unsigned int x, unsigned int y, byte color);

    void forceClear();
    void clear();
    void display();

    void destroyMIDIDevice();
    void setMIDIDevice(RtMidiOut* device);
    RtMidiOut* getMIDIDevice();

    ControllerOutput();
    ControllerOutput(RtMidiOut* device);
    ~ControllerOutput();


private:
    std::array<byte, 100> currentState;
    std::array<byte, 100> lastState;
    std::map<byte, byte> queuedStateChanges;
    RtMidiOut* midi;


};
