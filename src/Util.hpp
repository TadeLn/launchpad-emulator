#pragma once


#include "rtmidi/RtMidi.h"
#include "tutils/Pair.hpp"


typedef unsigned char byte;

namespace Util {


void listPorts(RtMidi* midi);
RtMidiIn* getInputMIDIConnection(RtMidiIn::RtMidiCallback cb, std::string portName = "Launchpad Emulator INPUT");
RtMidiOut* getOutputMIDIConnection(std::string portName = "Launchpad Emulator OUTPUT");

void getIntFromInput(int& a);

tu::Pair<unsigned int> pitchToCoords(byte pitch);
byte coordsToPitch(unsigned int x, unsigned int y);


}
