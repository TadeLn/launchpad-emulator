#include "Util.hpp"



void Util::listPorts(RtMidi* midi) {
    unsigned int nPorts = midi->getPortCount();
    std::cout << "\nAvaliable ports: (" << nPorts << ")\n";
    std::string portName;
    for (unsigned int i = 0; i < nPorts; i++) {
        try {
            portName = midi->getPortName(i);
        } catch (RtMidiError &error) {
            error.printMessage();
            return;
        }
        std::cout << "  Port #" << i << ": " << portName << '\n';
    }
}

RtMidiIn* Util::getInputMIDIConnection(RtMidiIn::RtMidiCallback cb, std::string portName) {
    RtMidiIn* midi;
    try {
        midi = new RtMidiIn();
        midi->setCallback(cb);
        int a = -2;
        while (a == -2) {
            std::cout << "\nChoose INPUT port";
            listPorts(midi);
            getIntFromInput(a);
        }

        if (a == -1) {
            midi->openVirtualPort(portName);
        } else {
            midi->openPort(a, portName);
        }
    } catch (RtMidiError& e) {
        e.printMessage();
        return nullptr;
    }
    return midi;
}

RtMidiOut* Util::getOutputMIDIConnection(std::string portName) {
    RtMidiOut* midi;
    try {
        midi = new RtMidiOut();
        int a = -2;
        while (a == -2) {
            std::cout << "\nChoose OUTPUT port:";
            listPorts(midi);
            getIntFromInput(a);
        }

        if (a == -1) {
            midi->openVirtualPort(portName);
        } else {
            midi->openPort(a, portName);
        }
    } catch (RtMidiError& e) {
        e.printMessage();
        return nullptr;
    }
    return midi;
}



void Util::getIntFromInput(int& a) {
    std::cout << "$ ";

    std::string input;
    getline(std::cin, input);
    if (input.empty()) a = -1;
    try {
        a = std::stoi(input);
        return;
    } catch (std::exception& e) {
        a = -2;
    }
}



tu::Pair<unsigned int> Util::pitchToCoords(byte pitch) {
    return tu::Pair<unsigned int>(pitch % 10, pitch / 10);
}

byte Util::coordsToPitch(unsigned int x, unsigned int y) {
    if (x > 9) x = 9;
    if (y > 9) y = 9;
    return x + (y * 10);
}
