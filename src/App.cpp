#include "App.hpp"

#include <thread>

#include "tutils/json/Object.hpp"

#include "Renderer.hpp"



int App::midiInChannel = 0;
int App::midiOutChannel = 0;



void App::start() {
    isRunning = true;

    reloadColors();

    {
        RtMidiOut* midiOut = Util::getOutputMIDIConnection();
        if (midiOut == nullptr) throw tu::Exception("MidiOut could not be created", EXCTX);
        out.setMIDIDevice(midiOut);
    }
    {
        RtMidiIn* midiIn = Util::getInputMIDIConnection([](double timestamp, std::vector<unsigned char>* message, void* userData){
            App::inst()->in.handleMIDIMessage(timestamp, *message);
        });
        if (midiIn == nullptr) throw tu::Exception("MidiIn could not be created", EXCTX);
        in.setMIDIDevice(midiIn);
    }


    r->init();

    while (isRunning) {
        loop();
    }
}

void App::loop() {
    ControllerInput::MidiMessage msg;
    while (in.pollMessage(msg)) {
        if (verboseInput) {
            std::cout << "Recieved message @ " << msg.timestamp << " \t| ";
            for (int i = 0; i < msg.data.size(); i++) {
                std::cout << (unsigned int)msg.data.at(i) << " ";
            }
        }

        const int channel = 5;

        byte messageType = msg.data.at(0);
        byte messageChannel = messageType & 0b00001111;
        byte messageTypeOnly = messageType & 0b11110000;

        if (verboseInput) {
            std::cout << " \t| Channel: " << (unsigned int)messageChannel << " Type: " << (unsigned int)messageTypeOnly << "\n";
        }

        if (messageChannel == channel) {
            
            if (messageTypeOnly == 144 || messageTypeOnly == 176) {
                byte pitch = msg.data.at(1);
                byte velocity = msg.data.at(2);

                // handle input here
            }
        }
    }

    // Main loop 

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

void App::exit() {
    isRunning = false;
}



void App::reloadColors() {
    try {
        auto json = tu::json::Node::fromFile("colors.json");
        auto arr = json->getOr<tu::json::Node::array>();

        int i = 0;
        for (auto x : arr) {
            auto type = x->getType();

            unsigned int number = 0;
            bool loaded = false;

            if (type == tu::json::Node::STRING) {
                std::string numberString = arr[i]->getOr<std::string>();
                try {
                    number = std::stoul(numberString, nullptr, 16);
                    loaded = true;
                } catch (std::exception& e) {
                    std::cerr << "Color #" << i << " \"" << numberString << "\" is in the incorrect format\n";
                }
            } else if (type == tu::json::Node::UINT) {
                auto result = arr[i]->get<unsigned int>();
                if (result.has_value()) {
                    number = result.value();
                    loaded = true;
                }
            }

            if (loaded) {
                sf::Color color = sf::Color(number << 8);
                color.a = 255;
                colors[i] = color;
            }

            i++;
            if (i >= 128) break;
        }
    } catch (tu::Exception& e) {
        e.print("Error while reloading: ");
    }
}



App::App() {
    App::i = this;
    this->r = new Renderer(this);
}

App::~App() {
    in.destroyMIDIDevice();
    out.forceClear();
    out.display();
    out.destroyMIDIDevice();
    delete r;
}

App* App::inst() {
    return App::i;
}

App* App::i;
