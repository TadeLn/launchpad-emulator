#pragma once


#include <array>
#include <queue>
#include <rtmidi/RtMidi.h>

#include "Util.hpp"



class ControllerInput {


public:
    class MidiMessage {

    public:
        double timestamp;
        std::vector<byte> data;

        MidiMessage() = default;
        MidiMessage(double timestamp, std::vector<byte> data);
    };

    void handleMIDIMessage(double timestamp, std::vector<byte> data);

    void destroyMIDIDevice();
    void setMIDIDevice(RtMidiIn* device);
    RtMidiIn* getMIDIDevice();

    bool pollMessage(MidiMessage&);

    bool isPressed(byte pitch);
    byte getVelocity(byte pitch);

    ControllerInput();
    ControllerInput(RtMidiIn* device);
    ~ControllerInput();


private:
    RtMidiIn* midi;

    std::queue<MidiMessage> unhandledMessages;
    std::array<byte, 256> currentValues;


};

