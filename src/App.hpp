#pragma once


#include <SFML/Graphics.hpp>

#include "ControllerInput.hpp"
#include "ControllerOutput.hpp"
#include "Renderer.hpp"

class App;



class App {

public:
    bool verboseInput = false;

    Renderer* r;

    ControllerInput in;
    ControllerOutput out;

    static int midiInChannel;
    static int midiOutChannel;

    void start();
    void loop();
    void exit();

    std::array<sf::Color, 128> colors;
    void reloadColors();

    App();
    ~App();

    static App* inst();

private:
    bool isRunning = false;
    static App* i;


};
