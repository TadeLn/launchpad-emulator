#include "Renderer.hpp"

#include <SFML/Graphics.hpp>

#include "App.hpp"



void Renderer::init() {
    this->t = std::thread([this](){
        this->start();
    });
}

Renderer::Renderer(App* a) {
    this->a = a;
}



void Renderer::updateButton(int x, int y, byte velocity) {
    x -= margin - (distance * 3 / 2);
    y -= margin - (distance * 3 / 2);

    int totalSize = distance + size;

    this->a->out.clear();

    int coordX = x / totalSize;
    int coordY = y / totalSize;

    if (coordX >= 0 && coordX <= 9 && coordY >= 0 && coordY <= 9) {
        this->a->out.setPixel(Util::coordsToPitch(coordX, 9 - coordY), velocity);
    }

    this->a->out.display();
}



void Renderer::start() {
    const int windowSize = (2 * margin) + (10 * size) + (9 * distance);

    sf::RenderWindow window(sf::VideoMode(windowSize, windowSize), "Launchpad Emulator");
    window.setVerticalSyncEnabled(true);

    sf::RectangleShape squareButton(sf::Vector2f(size, size));
    sf::CircleShape circleButton(size / 2.f);

    sf::Texture padTexture;
    padTexture.loadFromFile("pad.png");
    squareButton.setTexture(&padTexture);

    sf::Texture buttonTexture;
    buttonTexture.loadFromFile("button.png");
    circleButton.setTexture(&buttonTexture);



    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::Resized) {
                sf::View view;
                if (event.size.width < event.size.height) {
                    view = sf::View(
                        sf::Vector2f(windowSize / 2, windowSize / 2),    
                        sf::Vector2f(windowSize, ((double)windowSize / event.size.width) * event.size.height)
                    );
                } else {
                    view = sf::View(
                        sf::Vector2f(windowSize / 2, windowSize / 2),    
                        sf::Vector2f(((double)windowSize / event.size.height) * event.size.width, windowSize)
                    );
                }
                window.setView(view);
            }

            if (event.type == sf::Event::MouseButtonPressed || event.type == sf::Event::MouseButtonReleased || event.type == sf::Event::MouseMoved) {
                // Detect pressed buttons
                auto mousePos = sf::Mouse::getPosition(window);
                auto pos = window.mapPixelToCoords(sf::Vector2i(mousePos.x, mousePos.y));
                updateButton(pos.x, pos.y, sf::Mouse::isButtonPressed(sf::Mouse::Left) ? 127 : 0);
            }

            if (event.type == sf::Event::KeyPressed) {
                if (event.key.code == sf::Keyboard::R) {
                    padTexture.loadFromFile("pad.png");
                    buttonTexture.loadFromFile("button.png");
                    a->reloadColors();

                } else if (event.key.code == sf::Keyboard::V) {
                    a->verboseInput != a->verboseInput;
                }
            }
        }
        


        window.clear();

        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {

                if ((x == 0 && y == 0) || (x == 9 && y == 0) || (x == 0 && y == 9) || (x == 9 && y == 9)) {
                    // Draw nothing (corners)
                    continue;
                }

                sf::Color color = a->colors[a->in.getVelocity(Util::coordsToPitch(x, y))];

                if (x == 0 || y == 0 || x == 9 || y == 9)  {
                    // Draw circle button (edges)
                    circleButton.setPosition(
                        (margin - distance) + ((distance + size) * x),
                        (margin - distance) + ((distance + size) * (9 - y))
                    );
                    circleButton.setFillColor(color);
                    window.draw(circleButton);
                } else {
                    // Draw square pad
                    squareButton.setPosition(
                        (margin - distance) + ((distance + size) * x),
                        (margin - distance) + ((distance + size) * (9 - y))
                    );
                    squareButton.setFillColor(color);
                    window.draw(squareButton);
                }
            }
        }


        window.display();
    }

    a->exit();
}
